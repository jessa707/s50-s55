// To use react bootstrap components, you must first import them from the react-bootstrap package
import {Container, Navbar, Nav} from 'react-bootstrap'

export default function AppNavbar(){
  return(
    <Navbar bg="light" expand="lg">
          <Container fluid>
            <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#">Courses</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
      </Navbar>
  )
}
