import React, { useState } from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

export default function CourseCard({ course }) {
  const { name, description, price } = course;
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(10);
  const [femaleCount, setFemaleCount] = useState(0);
  const [maleCount, setMaleCount] = useState(0);

  function enroll() {
    if (seats > 0) {
      setCount(count + 1);
      setSeats(seats - 1);

      // Increment the appropriate gender count
      if (count % 2 === 0) {
        setFemaleCount(femaleCount + 1);
      } else {
        setMaleCount(maleCount + 1);
      }
    }
  }

  return (
    <Row className="my-2">
      <Col xs={12} md={8}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className="mb-1 text-muted">Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>

            <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
            <Card.Text>Php{price}</Card.Text>

            <Card.Text>Seats available: {seats}</Card.Text>
            <Card.Subtitle className="mb-2 text-muted">Enrollees:</Card.Subtitle>
            <Card.Text>Number: {count}</Card.Text>

            <Card.Text>Gender: {count % 2 === 0 ? 'Female' : 'Male'}</Card.Text>

            <Card.Subtitle className="mb-2 text-muted">Female:</Card.Subtitle>
            <Card.Text>Number: {femaleCount}</Card.Text>

            <Card.Subtitle className="mb-2 text-muted">Male:</Card.Subtitle>
            <Card.Text>Number: {maleCount}</Card.Text>

            <Button variant="danger" onClick={enroll} disabled={seats === 0}>
              Enroll
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  }).isRequired
};
