// Importables
import {Fragment} from 'react';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import {Container} from 'react-bootstrap';
import './App.css';

// Component function
function App() {
  // The component function returns JSX syntax that serves as the UI of the component.
  // Note: JSX syntax may look like HTML but it is actually Javascript that is formatted to look like HTML and is not actually HTML. The benefit of JSX is the ability to easily integrate Javascript with HTML syntax.
  return (
    // When rendering multiple components, they must always be enclosed in a parent component/element.
    <Fragment>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
    </Fragment>
  );
}

// Exporting of the component function
export default App;

